﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Vuforia;
using System.Collections.Generic;

public class Menu : MonoBehaviour {

	private GameObject loboFrontalDireito;
	private GameObject loboFrontalEsquerdo;
	private GameObject loboParitalDireito;
	private GameObject loboParitalEsquerdo;
	private GameObject loboOccipitalDireito;
	private GameObject loboOccipitalEsquerdo;
	private GameObject loboTemporalDireito;
	private GameObject loboTemporalEsquerdo;
	private GameObject cerebero;
	private GameObject glandulaPituitaria;
	private GameObject troncoEncefalico;

	private Color corLoboFrontal;
	private Color corLoboParital;
	private Color corLoboOccipital;
	private Color corLoboTemporal;
	private Color corCerebero;
	private Color corGlandulaPituitaria;
	private Color corTroncoEncefalico;
	private Color corPadrao;

	private bool corFoiAlterada = false;
	private bool alterarCor = false;

	private int posicaoXBotao = 15;
	private int posicaoYBotao = 10;
	private int alturaBotao = 100;
	private int larguraBotao = 250;
	private GUIStyle estiloBotao;



	void EsconderOuExibirObjeto(GameObject objeto1, GameObject objeto2 = null){

		objeto1.SetActive (!objeto1.activeSelf);

		if (objeto2 != null) {
			objeto2.SetActive (!objeto2.activeSelf);
		}
	}

	void ColorirObjeto(GameObject objeto1, Color cor, GameObject objeto2 = null){
		if (corFoiAlterada) {
			objeto1.GetComponent<Renderer> ().material.color = corPadrao;
			if (objeto2 != null) {
				objeto2.GetComponent<Renderer> ().material.color = corPadrao;
			}
		} else {
			objeto1.GetComponent<Renderer>().material.color = cor;
			if (objeto2 != null) {
				objeto2.GetComponent<Renderer> ().material.color = cor;
			}
		}

		corFoiAlterada = !corFoiAlterada;
	}

	void AplicarOuRemoverCoresCerebro() {
		if (alterarCor) {
			loboFrontalDireito.GetComponent <Renderer> ().material.color = corPadrao;
			loboFrontalEsquerdo.GetComponent <Renderer> ().material.color = corPadrao;
			loboParitalDireito.GetComponent <Renderer> ().material.color = corPadrao;
			loboParitalEsquerdo.GetComponent <Renderer> ().material.color = corPadrao;
			loboOccipitalDireito.GetComponent <Renderer> ().material.color = corPadrao;
			loboOccipitalEsquerdo.GetComponent <Renderer> ().material.color = corPadrao;
			loboTemporalDireito.GetComponent <Renderer> ().material.color = corPadrao;
			loboTemporalEsquerdo.GetComponent <Renderer> ().material.color = corPadrao;
			cerebero.GetComponent <Renderer> ().material.color = corPadrao;
			glandulaPituitaria.GetComponent <Renderer> ().material.color = corPadrao;
			troncoEncefalico.GetComponent <Renderer> ().material.color = corPadrao;
		} else {
			loboFrontalDireito.GetComponent <Renderer> ().material.color = corLoboFrontal;
			loboFrontalEsquerdo.GetComponent <Renderer> ().material.color = corLoboFrontal;
			loboParitalDireito.GetComponent <Renderer> ().material.color = corLoboParital;
			loboParitalEsquerdo.GetComponent <Renderer> ().material.color = corLoboParital;
			loboOccipitalDireito.GetComponent <Renderer> ().material.color = corLoboOccipital;
			loboOccipitalEsquerdo.GetComponent <Renderer> ().material.color = corLoboOccipital;
			loboTemporalDireito.GetComponent <Renderer> ().material.color = corLoboTemporal;
			loboTemporalEsquerdo.GetComponent <Renderer> ().material.color = corLoboTemporal;
			cerebero.GetComponent <Renderer> ().material.color = corCerebero;
			glandulaPituitaria.GetComponent <Renderer> ().material.color = corGlandulaPituitaria;
			troncoEncefalico.GetComponent <Renderer> ().material.color = corTroncoEncefalico;
		}
	}

	void CapturarCoresCerebro() {
		corPadrao = troncoEncefalico.GetComponent <Renderer> ().material.color;

		corLoboFrontal = loboFrontalDireito.GetComponent <Renderer> ().material.color;
		corLoboParital = loboParitalDireito.GetComponent <Renderer> ().material.color;
		corLoboOccipital = loboOccipitalDireito.GetComponent <Renderer> ().material.color;
		corLoboTemporal = loboTemporalDireito.GetComponent <Renderer> ().material.color;
		corCerebero = cerebero.GetComponent <Renderer> ().material.color;
		corGlandulaPituitaria = glandulaPituitaria.GetComponent <Renderer> ().material.color;
		corTroncoEncefalico = troncoEncefalico.GetComponent <Renderer> ().material.color;
	}


	void InicializarPartesCerebro ()
	{
		loboFrontalDireito = GameObject.FindGameObjectWithTag ("lobo_frontal_direito");
		loboFrontalEsquerdo = GameObject.FindGameObjectWithTag ("lobo_frontal_esquerdo");
		loboParitalDireito = GameObject.FindGameObjectWithTag ("lobo_parital_direito");
		loboParitalEsquerdo = GameObject.FindGameObjectWithTag ("lobo_parital_esquerdo");
		loboOccipitalDireito = GameObject.FindGameObjectWithTag ("lobo_occipital_direito");
		loboOccipitalEsquerdo = GameObject.FindGameObjectWithTag ("lobo_occipital_esquerdo");
		loboTemporalDireito = GameObject.FindGameObjectWithTag ("lobo_temporal_direito");
		loboTemporalEsquerdo = GameObject.FindGameObjectWithTag ("lobo_temporal_esquerdo");
		cerebero = GameObject.FindGameObjectWithTag ("cerebero");
		glandulaPituitaria = GameObject.FindGameObjectWithTag ("glandula_pituitaria");
		troncoEncefalico = GameObject.FindGameObjectWithTag ("tronco_encefalico");
	}

	static void ConfigurarTela ()
	{
		Screen.orientation = ScreenOrientation.LandscapeRight;
	}

	void Start () {
		InicializarPartesCerebro ();
		CapturarCoresCerebro ();
		ConfigurarTela ();
	}


	void Update () {
		
	}

	void InicializarLayoutBotao ()
	{
		estiloBotao = new GUIStyle (GUI.skin.button);
		estiloBotao.fontSize = 20;
		estiloBotao.font = Resources.Load<Font> ("Arial");
	}

	void BotoesCerebro ()
	{
		if (GUI.Button (new Rect (posicaoXBotao, posicaoYBotao, larguraBotao, alturaBotao), "LOBO FRONTAL", estiloBotao)) {
			if (alterarCor) {
				ColorirObjeto (loboFrontalDireito, corLoboFrontal, loboFrontalEsquerdo);
			}
			else {
				EsconderOuExibirObjeto (loboFrontalDireito, loboFrontalEsquerdo);
			}
		}
		if (GUI.Button (new Rect (posicaoXBotao, posicaoYBotao * 13, larguraBotao, alturaBotao), "LOBO PARITAL", estiloBotao)) {
			if (alterarCor) {
				ColorirObjeto (loboParitalDireito, corLoboParital, loboParitalEsquerdo);
			}
			else {
				EsconderOuExibirObjeto (loboParitalDireito, loboParitalEsquerdo);
			}
		}
		if (GUI.Button (new Rect (posicaoXBotao, posicaoYBotao * 26, larguraBotao, alturaBotao), "LOBO OCCIPITAL", estiloBotao)) {
			if (alterarCor) {
				ColorirObjeto (loboOccipitalDireito, corLoboOccipital, loboOccipitalEsquerdo);
			}
			else {
				EsconderOuExibirObjeto (loboOccipitalDireito, loboOccipitalEsquerdo);
			}
		}
		if (GUI.Button (new Rect (posicaoXBotao, posicaoYBotao * 39, larguraBotao, alturaBotao), "LOBO TEMPORAL", estiloBotao)) {
			if (alterarCor) {
				ColorirObjeto (loboTemporalDireito, corLoboTemporal, loboTemporalEsquerdo);
			}
			else {
				EsconderOuExibirObjeto (loboTemporalDireito, loboTemporalEsquerdo);
			}
		}
		if (GUI.Button (new Rect (posicaoXBotao, posicaoYBotao * 52, larguraBotao, alturaBotao), "CEREBERO", estiloBotao)) {
			if (alterarCor) {
				ColorirObjeto (cerebero, corCerebero);
			}
			else {
				EsconderOuExibirObjeto (cerebero);
			}
		}
		if (GUI.Button (new Rect (posicaoXBotao, posicaoYBotao * 65, larguraBotao, alturaBotao), "GLANDULA PITUITARIA", estiloBotao)) {
			if (alterarCor) {
				ColorirObjeto (glandulaPituitaria, corGlandulaPituitaria);
			}
			else {
				EsconderOuExibirObjeto (glandulaPituitaria);
			}
		}
		if (GUI.Button (new Rect (posicaoXBotao, posicaoYBotao * 78, larguraBotao, alturaBotao), "TRONCO ENCEFALICO", estiloBotao)) {
			if (alterarCor) {
				ColorirObjeto (troncoEncefalico, corTroncoEncefalico);
			}
			else {
				EsconderOuExibirObjeto (troncoEncefalico);
			}
		}
		if (GUI.Button (new Rect (1650, posicaoYBotao, larguraBotao, alturaBotao), "CORES", estiloBotao)) {
			alterarCor = !alterarCor;
			AplicarOuRemoverCoresCerebro ();
		}

	}


	void OnGUI () {
		InicializarLayoutBotao ();

		BotoesCerebro ();
	}
}
